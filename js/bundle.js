var camera;
var scene;
var renderer;
var cnt = 0;
init();
animate();
  
function init() {
  
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 1000);
    camera.aspect = 1;
  
    var light1 = new THREE.DirectionalLight( 0xffffff, .6 );
    light1.position.set( 0, 1, 1 ).normalize();
    scene.add(light1);

	var ambLight = new THREE.AmbientLight(0x404040);
	scene.add(ambLight);
    var light2 = new THREE.DirectionalLight(0xffffff, .51);
	light2.position.set(100, 100, 50);
    //scene.add(light2);

  
  	var dim = 60;
    var geometry = new THREE.CubeGeometry( dim, dim, dim);
    var material = new THREE.MeshPhongMaterial( { map: THREE.ImageUtils.loadTexture('images/winterM.png') } );
	var side1 = [new THREE.Vector2(0, .666), new THREE.Vector2(.5, .666), new THREE.Vector2(.5, 1), new THREE.Vector2(0, 1)];
	var side2 = [new THREE.Vector2(.5, .666), new THREE.Vector2(1, .666), new THREE.Vector2(1, 1), new THREE.Vector2(.5, 1)];
	var side3 = [new THREE.Vector2(0, .333), new THREE.Vector2(.5, .333), new THREE.Vector2(.5, .666), new THREE.Vector2(0, .666)];
	var side4 = [new THREE.Vector2(.5, .333), new THREE.Vector2(1, .333), new THREE.Vector2(1, .666), new THREE.Vector2(.5, .666)];
	var side5 = [new THREE.Vector2(0, 0), new THREE.Vector2(.5, 0), new THREE.Vector2(.5, .333), new THREE.Vector2(0, .333)];
	var side6 = [new THREE.Vector2(.5, 0), new THREE.Vector2(1, 0), new THREE.Vector2(1, .333), new THREE.Vector2(.5, .333)];

	geometry.faceVertexUvs[0] = [];
	geometry.faceVertexUvs[0][0] = [ side1[0], side1[1], side1[3] ];
	geometry.faceVertexUvs[0][1] = [ side1[1], side1[2], side1[3] ];
	  
	geometry.faceVertexUvs[0][2] = [ side2[0], side2[1], side2[3] ];
	geometry.faceVertexUvs[0][3] = [ side2[1], side2[2], side2[3] ];
	  
	geometry.faceVertexUvs[0][4] = [ side3[0], side3[1], side3[3] ];
	geometry.faceVertexUvs[0][5] = [ side3[1], side3[2], side3[3] ];
	  
	geometry.faceVertexUvs[0][6] = [ side4[0], side4[1], side4[3] ];
	geometry.faceVertexUvs[0][7] = [ side4[1], side4[2], side4[3] ];
	  
	geometry.faceVertexUvs[0][8] = [ side5[0], side5[1], side5[3] ];
	geometry.faceVertexUvs[0][9] = [ side5[1], side5[2], side5[3] ];
	  
	geometry.faceVertexUvs[0][10] = [ side6[0], side6[1], side6[3] ];
	geometry.faceVertexUvs[0][11] = [ side6[1], side6[2], side6[3] ];


  
    mesh = new THREE.Mesh(geometry, material );
    mesh.position.z = -50
    scene.add( mesh );
  
    renderer = new THREE.WebGLRenderer();
//    renderer.setSize( window.innerWidth, window.innerHeight );
	var ratio = 1.4;
    renderer.setSize( window.innerWidth/ratio, window.innerHeight/ratio );

   
    document.body.appendChild( renderer.domElement );
  
    render();
}
  
function animate() {
	cnt++;
	var x = (Math.sin(cnt/1000) * 0.003);
	mesh.rotation.x += x;
	mesh.rotation.y += .0005 - x;
	render();
	requestAnimationFrame( animate );
}
  
function render() {
	renderer.render( scene, camera );
}